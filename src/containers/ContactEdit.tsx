import React, { useState, lazy } from "react";
import gql from "graphql-tag";
import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField";
import { useMutation, useQuery } from "@apollo/react-hooks";
import { Contact } from "../services/contacts-api";
import { GET_CONTACTS } from "./ContactsList";
import { useParams } from "react-router";
import ContactEditView from "../components/ContactEditView";

const UPDATE_CONTACT = gql`
  mutation updateContact($contact: InputContact!) {
    updateContact(contact: $contact) {
      id
      name
      email
    }
  }
`;

export const GET_CONTACT = gql`
  query contact($id: ID!) {
    contact(id: $id) {
      id
      name
      email
    }
  }
`;

const ContactEdit = () => {
  const { id } = useParams();
  if (!id) {
    throw Error("An ID must be provided");
  }

  const { data: contactData, loading } = useQuery<{ contact: Contact }, {}>(
    GET_CONTACT,
    {
      variables: { id },
      skip: !id
    }
  );
  const [updateContact, { data }] = useMutation<
    { updateContact: Contact },
    { contact: Contact }
  >(UPDATE_CONTACT);

  if (loading) {
    return <p>Loading ...</p>;
  }

  if (!contactData || !contactData.contact) {
    throw new Error("Contact not found");
  }

  const onUpdateContact = (contact: Contact) => {
    updateContact({
      variables: {
        contact
      },
      refetchQueries: [{ query: GET_CONTACTS }]
    });
  };

  return (
    <>
      {contactData.contact && (
        <ContactEditView
          onUpdateContact={onUpdateContact}
          contact={contactData.contact}
        />
      )}
    </>
  );
};

export default ContactEdit;
