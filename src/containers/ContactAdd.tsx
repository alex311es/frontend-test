import React, { useState } from "react";
import gql from "graphql-tag";
import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField";
import { useMutation } from "@apollo/react-hooks";
import { Contact } from "../services/contacts-api";
import { GET_CONTACTS } from "./ContactsList";

const ADD_CONTACT = gql`
  mutation AddContact($contact: InputContact!) {
    addContact(contact: $contact) {
      id
      name
      email
    }
  }
`;

const ContactAdd = () => {
  const [name, setName] = useState<string>("");
  const [email, setEmail] = useState<string>("");

  const [addContact, { data }] = useMutation<
    { addContact: Contact },
    { contact: Omit<Contact, "id"> }
  >(ADD_CONTACT, {
    variables: { contact: { name, email } },
    refetchQueries: [{ query: GET_CONTACTS }]
  });

  const onChangeField = (callback: Function) => (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    event.preventDefault();
    callback(event.target.value);
  };

  const onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    addContact();
    setName("");
    setEmail("");
  };

  return (
    <form onSubmit={onSubmit}>
      <h2>Add a new contact</h2>
      <FormControl>
        <TextField
          id="name"
          placeholder="Enter the name"
          onChange={onChangeField(setName)}
        />
      </FormControl>
      <FormControl>
        <TextField
          id="email"
          placeholder="Enter the email"
          type="email"
          onChange={onChangeField(setEmail)}
        />
      </FormControl>

      <FormControl>
        <Button
          type="submit"
          variant="contained"
          color="primary"
          disabled={!name || !email}
        >
          Add Contact
        </Button>
      </FormControl>
    </form>
  );
};

export default ContactAdd;
