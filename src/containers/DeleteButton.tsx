import React from "react";
import IconButton from "@material-ui/core/IconButton";

import DeleteIcon from "@material-ui/icons/Delete";

import gql from "graphql-tag";
import { useMutation } from "@apollo/react-hooks";
import { GET_CONTACTS } from "./ContactsList";

const DELETE_CONTACT = gql`
  mutation deleteContact($id: ID!) {
    deleteContact(id: $id)
  }
`;

const DeleteButton = ({ id }: { id: string }) => {
  const [deleteContact] = useMutation(DELETE_CONTACT, {
    variables: { id },
    refetchQueries: [{ query: GET_CONTACTS }]
  });

  const onClick = () => deleteContact();

  return (
    <IconButton color="secondary" aria-label="delete">
      <DeleteIcon onClick={onClick} />
    </IconButton>
  );
};

export default DeleteButton;
