import React from "react";
import { useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";
import { Contacts } from "../services/contacts-api";

import ContactsListComponent from "../components/ContactsList";

export const GET_CONTACTS = gql`
  {
    contacts {
      id
      name
      email
    }
  }
`;

const ContactsList = () => {
  const { data } = useQuery<Contacts, {}>(GET_CONTACTS);
  return (
    <>
      {data && data.contacts && (
        <ContactsListComponent contacts={data.contacts || []} />
      )}
    </>
  );
};

export default ContactsList;
