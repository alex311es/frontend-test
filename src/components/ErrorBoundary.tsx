import React, { Component } from "react";

type State = {
  hasError: boolean;
  error?: Error;
};

export default class ErrorBoundary extends Component<{}, State> {
  constructor(props: any) {
    super(props);
    this.state = { hasError: false, error: undefined };
  }

  componentDidCatch(error: Error, info: any) {
    this.setState({ hasError: true, error });
  }

  render() {
    if (this.state.hasError && this.state.error) {
      return <p>{this.state.error.message}</p>;
    }

    return this.props.children;
  }
}
