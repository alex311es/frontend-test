import React from "react";
import { Contacts } from "../services/contacts-api";

import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import ContactComponent from "./Contact";

const ContactsList = ({ contacts }: Contacts) => {
  return (
    <Table>
      <TableHead>
        <TableRow>
          <TableCell component="th">Name</TableCell>
          <TableCell component="th">Email</TableCell>
          <TableCell component="th">Actions</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {contacts.map(contact => (
          <ContactComponent key={contact.id} contact={contact} />
        ))}
      </TableBody>
    </Table>
  );
};

export default ContactsList;
