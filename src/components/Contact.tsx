import React from "react";
import IconButton from "@material-ui/core/IconButton";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import EditIcon from "@material-ui/icons/Edit";
import { Contact } from "../services/contacts-api";
import DeleteButton from "../containers/DeleteButton";
import { Link } from "react-router-dom";

const ContactComponent = ({ contact }: { contact: Contact }) => {
  return (
    <TableRow key={contact.id}>
      <TableCell>{contact.name}</TableCell>
      <TableCell>{contact.email}</TableCell>
      <TableCell align="right">
        <Link to={`/edit/${contact.id}`}>
          <IconButton color="primary" aria-label="edit">
            <EditIcon />
          </IconButton>
        </Link>
        <DeleteButton id={contact.id} />
      </TableCell>
    </TableRow>
  );
};

export default ContactComponent;
