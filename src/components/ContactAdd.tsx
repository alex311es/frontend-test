import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField";

type Props = {
  onAddContact: (name: string, email: string) => void;
};

const ContactAdd = ({ onAddContact }: Props) => {
  const [name, setName] = useState<string>("");
  const [email, setEmail] = useState<string>("");

  const onChangeField = (callback: Function) => (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    event.preventDefault();
    callback(event.target.value);
  };

  const onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    onAddContact(name, email);
  };

  return (
    <form onSubmit={onSubmit}>
      <h2>Add a new contact</h2>
      <FormControl>
        <TextField
          id="name"
          placeholder="Enter the name"
          onChange={onChangeField(setName)}
        />
      </FormControl>
      <FormControl>
        <TextField
          id="email"
          placeholder="Enter the email"
          type="email"
          onChange={onChangeField(setEmail)}
        />
      </FormControl>

      <FormControl>
        <Button variant="contained" color="primary" disabled={!name || !email}>
          Add Contact
        </Button>
      </FormControl>
    </form>
  );
};

export default ContactAdd;
