import React from "react";
import { render } from "@testing-library/react";
import ContactsList from "./ContactsList";

describe("Issue", () => {
  let component: any;
  beforeAll(() => {
    const props = {
      contacts: [
        {
          id: "1",
          name: "one name",
          email: "email@example.com"
        }
      ]
    };
    component = render(<ContactsList {...props} />);
  });
  test("name must exists", () => {
    const nameEl = component.getByText("one name");
    expect(nameEl).toBeInTheDocument();
  });
  test("email must exists", () => {
    component.debug()
    const emailEl = component.getByText("email@example.com");
    expect(emailEl).toBeInTheDocument();
  });
});
