import React, { useState } from "react";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField";
import { useParams } from "react-router";
import { Link } from "react-router-dom";
import { Contact } from "../services/contacts-api";

type Props = {
  contact: Contact;
  onUpdateContact: Function;
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    buttons: {
      "& > *": {
        margin: theme.spacing(1)
      }
    }
  })
);

const ContactEditView = ({ contact, onUpdateContact }: Props) => {
  const classes = useStyles();

  const [name, setName] = useState<string>(contact.name);
  const [email, setEmail] = useState<string>(contact.email);

  const onChangeField = (callback: Function) => (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    event.preventDefault();
    callback(event.target.value);
  };

  const onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    onUpdateContact({ id: contact.id, name, email });
  };

  return (
    <form onSubmit={onSubmit}>
      <h2>Edit contact</h2>
      <FormControl fullWidth>
        <TextField
          id="name"
          placeholder="Enter the name"
          value={name}
          onChange={onChangeField(setName)}
        />
      </FormControl>
      <FormControl fullWidth>
        <TextField
          id="email"
          placeholder="Enter the email"
          value={email}
          type="email"
          onChange={onChangeField(setEmail)}
        />
      </FormControl>

      <FormControl className={classes.buttons}>
        <Button type="submit" variant="contained" color="primary" disabled={!name || !email}>
          Save Contact
        </Button>
        <Link to={`/`}>
          <Button variant="contained" color="secondary">
            Back
          </Button>
        </Link>
      </FormControl>
    </form>
  );
};

export default ContactEditView;
