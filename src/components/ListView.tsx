import React from "react";
import Grid from "@material-ui/core/Grid";
import ContactsList from "../containers/ContactsList";
import ContactAdd from "../containers/ContactAdd";

const ListView = () => {
  return (

      <header className="App-header">
        <h1>Contacts</h1>
        <Grid container spacing={2}>
          <Grid item xs={9}>
            <ContactsList />
          </Grid>
          <Grid item xs={3}>
            <ContactAdd />
          </Grid>
        </Grid>
      </header>
  );
};

export default ListView;
