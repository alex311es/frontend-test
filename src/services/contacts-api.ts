import ApolloClient from "apollo-boost";
import { InMemoryCache } from "apollo-cache-inmemory";

export type Contact = {
  id: string;
  name: string;
  email: string;
};

export type Contacts = {
  contacts: Contact[];
};

type ContactsApiService = {
  client: ApolloClient<InMemoryCache>;
};

export const createContactsApi = (uri: string): ContactsApiService => {
  const client: ApolloClient<InMemoryCache> = new ApolloClient({
    uri,
    cache: new InMemoryCache()
  });

  return {
    client
  };
};
