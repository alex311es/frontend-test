import React, { Component } from "react";
import { ApolloProvider } from "@apollo/react-hooks";
import Container from "@material-ui/core/Container";
import { createContactsApi } from "./services/contacts-api";
import "./App.css";
import ErrorBoundary from "./components/ErrorBoundary";
import ListView from "./components/ListView";
import ContactEdit from "./containers/ContactEdit";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

const { client } = createContactsApi("http://localhost:3001");

class App extends Component {
  render() {
    return (
      <ApolloProvider client={client}>
        <Container fixed>
          <div className="App">
            <Router>
              <Switch>
                <Route path="/edit/:id">
                  <ErrorBoundary>
                    <ContactEdit />
                  </ErrorBoundary>
                </Route>
                <Route exact path="/">
                  <ErrorBoundary>
                    <ListView />
                  </ErrorBoundary>
                </Route>
              </Switch>
            </Router>
          </div>
        </Container>
      </ApolloProvider>
    );
  }
}

export default App;
